/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/

#ifndef Sterownik_H
#define Sterownik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsDetektorBledu
#include "DetektorBledu.h"
//## link itsGrzalka
#include "Grzalka.h"
//## link itsMlynek
#include "Mlynek.h"
//## link itsPanelSterowania
#include "panelSterowania.h"
//## link itsPompa
#include "Pompa.h"
//## link itsSpieniacz
#include "Spieniacz.h"
//## link itsUbijacz
#include "Ubijacz.h"
//## link itsEkspres
class Ekspres;

//## package Default

//## class Sterownik
class Sterownik : public OMThread, public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSterownik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Sterownik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Sterownik();
    
    ////    Operations    ////
    
    //## operation ustawObjetosc(int)
    void ustawObjetosc(int volume = 50);
    
    //## operation ustawStan(Stany)
    void ustawStan(const Stany& state);
    
    ////    Additional operations    ////
    
    //## auto_generated
    DetektorBledu* getItsDetektorBledu() const;
    
    //## auto_generated
    Ekspres* getItsEkspres() const;
    
    //## auto_generated
    void setItsEkspres(Ekspres* p_Ekspres);
    
    //## auto_generated
    Grzalka* getItsGrzalka() const;
    
    //## auto_generated
    Mlynek* getItsMlynek() const;
    
    //## auto_generated
    panelSterowania* getItsPanelSterowania() const;
    
    //## auto_generated
    Pompa* getItsPompa() const;
    
    //## auto_generated
    Spieniacz* getItsSpieniacz() const;
    
    //## auto_generated
    Ubijacz* getItsUbijacz() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    Stany getStan() const;
    
    //## auto_generated
    void setStan(Stany p_stan);
    
    //## auto_generated
    int getWybor() const;
    
    //## auto_generated
    void setWybor(int p_wybor);
    
    //## auto_generated
    int getZadanaIlosc() const;
    
    //## auto_generated
    void setZadanaIlosc(int p_zadanaIlosc);
    
    //## auto_generated
    int getZadanaObjetosc() const;
    
    //## auto_generated
    void setZadanaObjetosc(int p_zadanaObjetosc);
    
    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);
    
    ////    Attributes    ////
    
    Stany stan;		//## attribute stan
    
    int wybor;		//## attribute wybor
    
    int zadanaIlosc;		//## attribute zadanaIlosc
    
    int zadanaObjetosc;		//## attribute zadanaObjetosc
    
    ////    Relations and components    ////
    
    DetektorBledu itsDetektorBledu;		//## link itsDetektorBledu
    
    Ekspres* itsEkspres;		//## link itsEkspres
    
    Grzalka itsGrzalka;		//## link itsGrzalka
    
    Mlynek itsMlynek;		//## link itsMlynek
    
    panelSterowania itsPanelSterowania;		//## link itsPanelSterowania
    
    Pompa itsPompa;		//## link itsPompa
    
    Spieniacz itsSpieniacz;		//## link itsSpieniacz
    
    Ubijacz itsUbijacz;		//## link itsUbijacz
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsEkspres(Ekspres* p_Ekspres);
    
    //## auto_generated
    void _setItsEkspres(Ekspres* p_Ekspres);
    
    //## auto_generated
    void _clearItsEkspres();
    
    //## auto_generated
    virtual void destroy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    inline bool rootState_isCompleted();
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Woda:
    //## statechart_method
    inline bool Woda_IN() const;
    
    //## statechart_method
    inline bool Woda_isCompleted();
    
    //## statechart_method
    void Woda_entDef();
    
    //## statechart_method
    void Woda_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Woda_handleEvent();
    
    // terminationstate_4:
    //## statechart_method
    inline bool terminationstate_4_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus terminationstate_4_handleEvent();
    
    // sendaction_7:
    //## statechart_method
    inline bool sendaction_7_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_7_handleEvent();
    
    // sendaction_5:
    //## statechart_method
    inline bool sendaction_5_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_5_handleEvent();
    
    // sendaction_3:
    //## statechart_method
    inline bool sendaction_3_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_3_handleEvent();
    
    // Objetosc:
    //## statechart_method
    inline bool Objetosc_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Objetosc_handleEvent();
    
    // terminationstate_20:
    //## statechart_method
    inline bool terminationstate_20_IN() const;
    
    // Spienianie:
    //## statechart_method
    inline bool Spienianie_IN() const;
    
    //## statechart_method
    inline bool Spienianie_isCompleted();
    
    //## statechart_method
    void Spienianie_entDef();
    
    //## statechart_method
    void Spienianie_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Spienianie_handleEvent();
    
    // terminationstate_15:
    //## statechart_method
    inline bool terminationstate_15_IN() const;
    
    // sendaction_19:
    //## statechart_method
    inline bool sendaction_19_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_19_handleEvent();
    
    // sendaction_17:
    //## statechart_method
    inline bool sendaction_17_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_17_handleEvent();
    
    // sendaction_16:
    //## statechart_method
    inline bool sendaction_16_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_16_handleEvent();
    
    // sendaction_14:
    //## statechart_method
    inline bool sendaction_14_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_14_handleEvent();
    
    // Spienianie_Objetosc:
    //## statechart_method
    inline bool Spienianie_Objetosc_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Spienianie_Objetosc_handleEvent();
    
    // Oczekiwanie:
    //## statechart_method
    inline bool Oczekiwanie_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Oczekiwanie_handleEvent();
    
    // Mielenie:
    //## statechart_method
    inline bool Mielenie_IN() const;
    
    //## statechart_method
    inline bool Mielenie_isCompleted();
    
    //## statechart_method
    void Mielenie_entDef();
    
    //## statechart_method
    void Mielenie_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Mielenie_handleEvent();
    
    // terminationstate_10:
    //## statechart_method
    inline bool terminationstate_10_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus terminationstate_10_handleEvent();
    
    // sendaction_9:
    //## statechart_method
    inline bool sendaction_9_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_9_handleEvent();
    
    // sendaction_12:
    //## statechart_method
    inline bool sendaction_12_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_12_handleEvent();
    
    // sendaction_11:
    //## statechart_method
    inline bool sendaction_11_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_11_handleEvent();
    
    // Inicjalizacja:
    //## statechart_method
    inline bool Inicjalizacja_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Sterownik_Enum {
        OMNonState = 0,
        Woda = 1,
        terminationstate_4 = 2,
        sendaction_7 = 3,
        sendaction_5 = 4,
        sendaction_3 = 5,
        Objetosc = 6,
        terminationstate_20 = 7,
        Spienianie = 8,
        terminationstate_15 = 9,
        sendaction_19 = 10,
        sendaction_17 = 11,
        sendaction_16 = 12,
        sendaction_14 = 13,
        Spienianie_Objetosc = 14,
        Oczekiwanie = 15,
        Mielenie = 16,
        terminationstate_10 = 17,
        sendaction_9 = 18,
        sendaction_12 = 19,
        sendaction_11 = 20,
        Inicjalizacja = 21
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int Woda_subState;
    
    int Spienianie_subState;
    
    IOxfTimeout* Spienianie_timeout;
    
    int Mielenie_subState;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSterownik : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Sterownik, OMAnimatedSterownik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Woda_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_4_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_7_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_5_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_3_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Objetosc_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_20_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Spienianie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_15_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_19_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_17_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_16_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_14_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Spienianie_Objetosc_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Oczekiwanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Mielenie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_10_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_9_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_12_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_11_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Inicjalizacja_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Sterownik::rootState_IN() const {
    return true;
}

inline bool Sterownik::rootState_isCompleted() {
    return ( IS_IN(terminationstate_20) );
}

inline bool Sterownik::Woda_IN() const {
    return rootState_subState == Woda;
}

inline bool Sterownik::Woda_isCompleted() {
    return ( IS_IN(terminationstate_4) );
}

inline bool Sterownik::terminationstate_4_IN() const {
    return Woda_subState == terminationstate_4;
}

inline bool Sterownik::sendaction_7_IN() const {
    return Woda_subState == sendaction_7;
}

inline bool Sterownik::sendaction_5_IN() const {
    return Woda_subState == sendaction_5;
}

inline bool Sterownik::sendaction_3_IN() const {
    return Woda_subState == sendaction_3;
}

inline bool Sterownik::Objetosc_IN() const {
    return Woda_subState == Objetosc;
}

inline bool Sterownik::terminationstate_20_IN() const {
    return rootState_subState == terminationstate_20;
}

inline bool Sterownik::Spienianie_IN() const {
    return rootState_subState == Spienianie;
}

inline bool Sterownik::Spienianie_isCompleted() {
    return ( IS_IN(terminationstate_15) );
}

inline bool Sterownik::terminationstate_15_IN() const {
    return Spienianie_subState == terminationstate_15;
}

inline bool Sterownik::sendaction_19_IN() const {
    return Spienianie_subState == sendaction_19;
}

inline bool Sterownik::sendaction_17_IN() const {
    return Spienianie_subState == sendaction_17;
}

inline bool Sterownik::sendaction_16_IN() const {
    return Spienianie_subState == sendaction_16;
}

inline bool Sterownik::sendaction_14_IN() const {
    return Spienianie_subState == sendaction_14;
}

inline bool Sterownik::Spienianie_Objetosc_IN() const {
    return Spienianie_subState == Spienianie_Objetosc;
}

inline bool Sterownik::Oczekiwanie_IN() const {
    return rootState_subState == Oczekiwanie;
}

inline bool Sterownik::Mielenie_IN() const {
    return rootState_subState == Mielenie;
}

inline bool Sterownik::Mielenie_isCompleted() {
    return ( IS_IN(terminationstate_10) );
}

inline bool Sterownik::terminationstate_10_IN() const {
    return Mielenie_subState == terminationstate_10;
}

inline bool Sterownik::sendaction_9_IN() const {
    return Mielenie_subState == sendaction_9;
}

inline bool Sterownik::sendaction_12_IN() const {
    return Mielenie_subState == sendaction_12;
}

inline bool Sterownik::sendaction_11_IN() const {
    return Mielenie_subState == sendaction_11;
}

inline bool Sterownik::Inicjalizacja_IN() const {
    return rootState_subState == Inicjalizacja;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/
