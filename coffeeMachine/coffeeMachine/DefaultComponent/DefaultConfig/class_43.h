/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_43
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/class_43.h
*********************************************************************/

#ifndef class_43_H
#define class_43_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## class class_43
class class_43 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedclass_43;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    class_43();
    
    //## auto_generated
    ~class_43();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedclass_43 : virtual public AOMInstance {
    DECLARE_META(class_43, OMAnimatedclass_43)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/class_43.h
*********************************************************************/
