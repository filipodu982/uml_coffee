/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ustawienia
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Ustawienia.h"
//## link itsEkspres
#include "Ekspres.h"
//#[ ignore
#define Default_Ustawienia_Ustawienia_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Ustawienia
Ustawienia::Ustawienia() {
    NOTIFY_ACTIVE_NOT_REACTIVE_CONSTRUCTOR(Ustawienia, Ustawienia(), 0, Default_Ustawienia_Ustawienia_SERIALIZE);
    itsEkspres = NULL;
}

Ustawienia::~Ustawienia() {
    NOTIFY_DESTRUCTOR(~Ustawienia, true);
    cleanUpRelations();
}

Ekspres* Ustawienia::getItsEkspres() const {
    return itsEkspres;
}

void Ustawienia::setItsEkspres(Ekspres* p_Ekspres) {
    if(p_Ekspres != NULL)
        {
            p_Ekspres->_addItsUstawienia(this);
        }
    _setItsEkspres(p_Ekspres);
}

void Ustawienia::cleanUpRelations() {
    if(itsEkspres != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsEkspres");
            Ekspres* current = itsEkspres;
            if(current != NULL)
                {
                    current->_removeItsUstawienia(this);
                }
            itsEkspres = NULL;
        }
}

void Ustawienia::__setItsEkspres(Ekspres* p_Ekspres) {
    itsEkspres = p_Ekspres;
    if(p_Ekspres != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsEkspres", p_Ekspres, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsEkspres");
        }
}

void Ustawienia::_setItsEkspres(Ekspres* p_Ekspres) {
    if(itsEkspres != NULL)
        {
            itsEkspres->_removeItsUstawienia(this);
        }
    __setItsEkspres(p_Ekspres);
}

void Ustawienia::_clearItsEkspres() {
    NOTIFY_RELATION_CLEARED("itsEkspres");
    itsEkspres = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedUstawienia::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsEkspres", false, true);
    if(myReal->itsEkspres)
        {
            aomsRelations->ADD_ITEM(myReal->itsEkspres);
        }
}
//#]

IMPLEMENT_META_P(Ustawienia, Default, Default, false, OMAnimatedUstawienia)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/
