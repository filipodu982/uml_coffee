/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DetektorBledu
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/DetektorBledu.h
*********************************************************************/

#ifndef DetektorBledu_H
#define DetektorBledu_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class DetektorBledu
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class DetektorBledu
class DetektorBledu : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDetektorBledu;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    DetektorBledu(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~DetektorBledu();
    
    ////    Operations    ////
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation sprawdzPoziomFusow()
    bool sprawdzPoziomFusow();
    
    //## operation sprawdzPoziomKawy()
    bool sprawdzPoziomKawy();
    
    //## operation sprawdzPoziomMleka()
    bool sprawdzPoziomMleka();
    
    //## operation sprawdzPoziomWody()
    bool sprawdzPoziomWody();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string ustawienia);

protected :

    //## operation ustawStan(Stany)
    void ustawStan(const Stany& state);
    
    ////    Additional operations    ////

public :

    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    bool getBlad() const;
    
    //## auto_generated
    void setBlad(bool p_blad);
    
    //## auto_generated
    Stany getStan() const;
    
    //## auto_generated
    void setStan(Stany p_stan);
    
    ////    Attributes    ////

protected :

    bool blad;		//## attribute blad
    
    Stany stan;		//## attribute stan
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // Woda:
    //## statechart_method
    inline bool Woda_IN() const;
    
    // sendaction_7:
    //## statechart_method
    inline bool sendaction_7_IN() const;
    
    // sendaction_6:
    //## statechart_method
    inline bool sendaction_6_IN() const;
    
    // Mleko:
    //## statechart_method
    inline bool Mleko_IN() const;
    
    // Kawa:
    //## statechart_method
    inline bool Kawa_IN() const;
    
    // Fusy:
    //## statechart_method
    inline bool Fusy_IN() const;
    
    // Detekcja:
    //## statechart_method
    inline bool Detekcja_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum DetektorBledu_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        Woda = 2,
        sendaction_7 = 3,
        sendaction_6 = 4,
        Mleko = 5,
        Kawa = 6,
        Fusy = 7,
        Detekcja = 8
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDetektorBledu : public OMAnimatedModul {
    DECLARE_REACTIVE_META(DetektorBledu, OMAnimatedDetektorBledu)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Woda_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_7_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_6_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Mleko_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Kawa_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Fusy_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Detekcja_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool DetektorBledu::rootState_IN() const {
    return true;
}

inline bool DetektorBledu::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool DetektorBledu::Woda_IN() const {
    return rootState_subState == Woda;
}

inline bool DetektorBledu::sendaction_7_IN() const {
    return rootState_subState == sendaction_7;
}

inline bool DetektorBledu::sendaction_6_IN() const {
    return rootState_subState == sendaction_6;
}

inline bool DetektorBledu::Mleko_IN() const {
    return rootState_subState == Mleko;
}

inline bool DetektorBledu::Kawa_IN() const {
    return rootState_subState == Kawa;
}

inline bool DetektorBledu::Fusy_IN() const {
    return rootState_subState == Fusy;
}

inline bool DetektorBledu::Detekcja_IN() const {
    return rootState_subState == Detekcja;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/DetektorBledu.h
*********************************************************************/
