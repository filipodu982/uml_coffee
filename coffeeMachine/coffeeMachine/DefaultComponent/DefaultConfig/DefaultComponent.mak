
############# Target type (Debug/Release) ##################
############################################################
CPPCompileDebug=-g
CPPCompileRelease=-O
LinkDebug=-g
LinkRelease=-O

CleanupFlagForSimulink=
SIMULINK_CONFIG=False
ifeq ($(SIMULINK_CONFIG),True)
CleanupFlagForSimulink=-DOM_WITH_CLEANUP
endif

ConfigurationCPPCompileSwitches=   $(INCLUDE_QUALIFIER). $(INCLUDE_QUALIFIER)$(OMROOT) $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/oxf $(DEFINE_QUALIFIER)CYGWIN $(INST_FLAGS) $(INCLUDE_PATH) $(INST_INCLUDES) -Wno-write-strings $(CPPCompileDebug) -c  $(CleanupFlagForSimulink)
ConfigurationCCCompileSwitches=$(INCLUDE_PATH) -c 

#########################################
###### Predefined macros ################
RM=/bin/rm -rf
INCLUDE_QUALIFIER=-I
DEFINE_QUALIFIER=-D
CC=g++
LIB_CMD=ar
LINK_CMD=g++
LIB_FLAGS=rvu
LINK_FLAGS= $(LinkDebug)   

#########################################
####### Context macros ##################

FLAGSFILE=
RULESFILE=
OMROOT="C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share"
RHPROOT="C:/Program Files/IBM/Rational/Rhapsody/8.3.1"

CPP_EXT=.cpp
H_EXT=.h
OBJ_EXT=.o
EXE_EXT=.exe
LIB_EXT=.a

INSTRUMENTATION=Animation

TIME_MODEL=RealTime

TARGET_TYPE=Executable

TARGET_NAME=DefaultComponent

all : $(TARGET_NAME)$(EXE_EXT) DefaultComponent.mak

TARGET_MAIN=MainDefaultComponent

LIBS=

INCLUDE_PATH= \
  $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/osconfig/Cygwin

ADDITIONAL_OBJS=

OBJS= \
  panelSterowania.o \
  Sterownik.o \
  Grzalka.o \
  Pompa.o \
  Mlynek.o \
  Ubijacz.o \
  Spieniacz.o \
  DetektorBledu.o \
  Ekspres.o \
  Modul.o \
  Ustawienia.o \
  Default.o




#########################################
####### Predefined macros ###############
$(OBJS) : $(INST_LIBS) $(OXF_LIBS)

ifeq ($(INSTRUMENTATION),Animation)

INST_FLAGS=$(DEFINE_QUALIFIER)OMANIMATOR $(DEFINE_QUALIFIER)__USE_W32_SOCKETS 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinaomanim$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),Tracing)

INST_FLAGS=$(DEFINE_QUALIFIER)OMTRACER 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwintomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinaomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),None)

INST_FLAGS= 
INST_INCLUDES=
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxsim$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxf$(LIB_EXT)
SOCK_LIB=-lws2_32

else
	@echo An invalid Instrumentation $(INSTRUMENTATION) is specified.
	exit
endif
endif
endif

.SUFFIXES: $(CPP_EXT)

#####################################################################
##################### Context dependencies and commands #############






panelSterowania.o : panelSterowania.cpp panelSterowania.h    Default.h Sterownik.h Modul.h 
	@echo Compiling panelSterowania.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o panelSterowania.o panelSterowania.cpp




Sterownik.o : Sterownik.cpp Sterownik.h    Default.h Ekspres.h panelSterowania.h Mlynek.h Grzalka.h Pompa.h Spieniacz.h Ubijacz.h DetektorBledu.h Modul.h 
	@echo Compiling Sterownik.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Sterownik.o Sterownik.cpp




Grzalka.o : Grzalka.cpp Grzalka.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Grzalka.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Grzalka.o Grzalka.cpp




Pompa.o : Pompa.cpp Pompa.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Pompa.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Pompa.o Pompa.cpp




Mlynek.o : Mlynek.cpp Mlynek.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Mlynek.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Mlynek.o Mlynek.cpp




Ubijacz.o : Ubijacz.cpp Ubijacz.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Ubijacz.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Ubijacz.o Ubijacz.cpp




Spieniacz.o : Spieniacz.cpp Spieniacz.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Spieniacz.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Spieniacz.o Spieniacz.cpp




DetektorBledu.o : DetektorBledu.cpp DetektorBledu.h    Default.h Sterownik.h Modul.h 
	@echo Compiling DetektorBledu.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o DetektorBledu.o DetektorBledu.cpp




Ekspres.o : Ekspres.cpp Ekspres.h    Default.h Sterownik.h Ustawienia.h Modul.h 
	@echo Compiling Ekspres.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Ekspres.o Ekspres.cpp




Modul.o : Modul.cpp Modul.h    Default.h 
	@echo Compiling Modul.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Modul.o Modul.cpp




Ustawienia.o : Ustawienia.cpp Ustawienia.h    Default.h Ekspres.h 
	@echo Compiling Ustawienia.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Ustawienia.o Ustawienia.cpp




Default.o : Default.cpp Default.h    panelSterowania.h Sterownik.h Grzalka.h Pompa.h Mlynek.h Ubijacz.h Spieniacz.h DetektorBledu.h Ekspres.h Modul.h Ustawienia.h 
	@echo Compiling Default.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Default.o Default.cpp







$(TARGET_MAIN)$(OBJ_EXT) : $(TARGET_MAIN)$(CPP_EXT) $(OBJS)
	@echo Compiling $(TARGET_MAIN)$(CPP_EXT)
	@$(CC) $(ConfigurationCPPCompileSwitches) -o  $(TARGET_MAIN)$(OBJ_EXT) $(TARGET_MAIN)$(CPP_EXT)

####################################################################
############## Predefined Instructions #############################
$(TARGET_NAME)$(EXE_EXT): $(OBJS) $(ADDITIONAL_OBJS) $(TARGET_MAIN)$(OBJ_EXT) DefaultComponent.mak
	@echo Linking $(TARGET_NAME)$(EXE_EXT)
	@$(LINK_CMD)  $(TARGET_MAIN)$(OBJ_EXT) $(OBJS) $(ADDITIONAL_OBJS) \
	$(LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(SOCK_LIB) \
	$(LINK_FLAGS) -o $(TARGET_NAME)$(EXE_EXT)

$(TARGET_NAME)$(LIB_EXT) : $(OBJS) $(ADDITIONAL_OBJS) DefaultComponent.mak
	@echo Building library $@
	@$(LIB_CMD) $(LIB_FLAGS) $(TARGET_NAME)$(LIB_EXT) $(OBJS) $(ADDITIONAL_OBJS)



clean:
	@echo Cleanup
	$(RM) panelSterowania.o
	$(RM) Sterownik.o
	$(RM) Grzalka.o
	$(RM) Pompa.o
	$(RM) Mlynek.o
	$(RM) Ubijacz.o
	$(RM) Spieniacz.o
	$(RM) DetektorBledu.o
	$(RM) Ekspres.o
	$(RM) Modul.o
	$(RM) Ustawienia.o
	$(RM) Default.o
	$(RM) $(TARGET_MAIN)$(OBJ_EXT) $(ADDITIONAL_OBJS)
	$(RM) $(TARGET_NAME)$(LIB_EXT)
	$(RM) $(TARGET_NAME)$(EXE_EXT)

