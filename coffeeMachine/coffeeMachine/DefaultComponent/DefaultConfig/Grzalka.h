/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Grzalka
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Grzalka.h
*********************************************************************/

#ifndef Grzalka_H
#define Grzalka_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Grzalka
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class Grzalka
class Grzalka : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedGrzalka;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Grzalka(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Grzalka();
    
    ////    Operations    ////
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation grzanie()
    void grzanie();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string ustawienia);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    int getTemperatura() const;
    
    //## auto_generated
    void setTemperatura(int p_temperatura);
    
    ////    Attributes    ////

protected :

    int temperatura;		//## attribute temperatura
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczona:
    //## statechart_method
    inline bool Wylaczona_IN() const;
    
    // sendaction_2:
    //## statechart_method
    inline bool sendaction_2_IN() const;
    
    // Grzanie:
    //## statechart_method
    inline bool Grzanie_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Grzalka_Enum {
        OMNonState = 0,
        Wylaczona = 1,
        sendaction_2 = 2,
        Grzanie = 3
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedGrzalka : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Grzalka, OMAnimatedGrzalka)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczona_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Grzanie_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Grzalka::rootState_IN() const {
    return true;
}

inline bool Grzalka::Wylaczona_IN() const {
    return rootState_subState == Wylaczona;
}

inline bool Grzalka::sendaction_2_IN() const {
    return rootState_subState == sendaction_2;
}

inline bool Grzalka::Grzanie_IN() const {
    return rootState_subState == Grzanie;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Grzalka.h
*********************************************************************/
