/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "DetektorBledu.h"
//## auto_generated
#include "Ekspres.h"
//## auto_generated
#include "Grzalka.h"
//## auto_generated
#include "Mlynek.h"
//## auto_generated
#include "Modul.h"
//## auto_generated
#include "panelSterowania.h"
//## auto_generated
#include "Pompa.h"
//## auto_generated
#include "Spieniacz.h"
//## auto_generated
#include "Sterownik.h"
//## auto_generated
#include "Ubijacz.h"
//## auto_generated
#include "Ustawienia.h"
//#[ ignore
#define eventmessage_0_SERIALIZE OM_NO_OP

#define eventmessage_0_UNSERIALIZE OM_NO_OP

#define eventmessage_0_CONSTRUCTOR eventmessage_0()

#define evClick_SERIALIZE OM_NO_OP

#define evClick_UNSERIALIZE OM_NO_OP

#define evClick_CONSTRUCTOR evClick()

#define evImpuls_SERIALIZE OM_NO_OP

#define evImpuls_UNSERIALIZE OM_NO_OP

#define evImpuls_CONSTRUCTOR evImpuls()

#define evStart_SERIALIZE OMADD_SER(state, x2String((int)myEvent->state))

#define evStart_UNSERIALIZE OMADD_UNSER(int, state, OMDestructiveString2X)

#define evStart_CONSTRUCTOR evStart(state)

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()

#define evBlad_SERIALIZE OM_NO_OP

#define evBlad_UNSERIALIZE OM_NO_OP

#define evBlad_CONSTRUCTOR evBlad()

#define evStartWoda_SERIALIZE OM_NO_OP

#define evStartWoda_UNSERIALIZE OM_NO_OP

#define evStartWoda_CONSTRUCTOR evStartWoda()

#define evStartMleko_SERIALIZE OM_NO_OP

#define evStartMleko_UNSERIALIZE OM_NO_OP

#define evStartMleko_CONSTRUCTOR evStartMleko()

#define evDone_SERIALIZE OM_NO_OP

#define evDone_UNSERIALIZE OM_NO_OP

#define evDone_CONSTRUCTOR evDone()

#define evTestowanie_SERIALIZE OM_NO_OP

#define evTestowanie_UNSERIALIZE OM_NO_OP

#define evTestowanie_CONSTRUCTOR evTestowanie()
//#]

//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event eventmessage_0()
eventmessage_0::eventmessage_0() {
    NOTIFY_EVENT_CONSTRUCTOR(eventmessage_0)
    setId(eventmessage_0_Default_id);
}

bool eventmessage_0::isTypeOf(const short id) const {
    return (eventmessage_0_Default_id==id);
}

IMPLEMENT_META_EVENT_P(eventmessage_0, Default, Default, eventmessage_0())

//## event evClick()
evClick::evClick() {
    NOTIFY_EVENT_CONSTRUCTOR(evClick)
    setId(evClick_Default_id);
}

bool evClick::isTypeOf(const short id) const {
    return (evClick_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evClick, Default, Default, evClick())

//## event evImpuls()
evImpuls::evImpuls() {
    NOTIFY_EVENT_CONSTRUCTOR(evImpuls)
    setId(evImpuls_Default_id);
}

bool evImpuls::isTypeOf(const short id) const {
    return (evImpuls_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evImpuls, Default, Default, evImpuls())

//## event evStart(Stany)
//#[ ignore
evStart::evStart(int p_state) : state((Stany)p_state) {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}
//#]

evStart::evStart(Stany p_state) : state(p_state) {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, Default, Default, evStart(Stany))

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_Default_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, Default, Default, evStop())

//## event evBlad()
evBlad::evBlad() {
    NOTIFY_EVENT_CONSTRUCTOR(evBlad)
    setId(evBlad_Default_id);
}

bool evBlad::isTypeOf(const short id) const {
    return (evBlad_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evBlad, Default, Default, evBlad())

//## event evStartWoda()
evStartWoda::evStartWoda() {
    NOTIFY_EVENT_CONSTRUCTOR(evStartWoda)
    setId(evStartWoda_Default_id);
}

bool evStartWoda::isTypeOf(const short id) const {
    return (evStartWoda_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStartWoda, Default, Default, evStartWoda())

//## event evStartMleko()
evStartMleko::evStartMleko() {
    NOTIFY_EVENT_CONSTRUCTOR(evStartMleko)
    setId(evStartMleko_Default_id);
}

bool evStartMleko::isTypeOf(const short id) const {
    return (evStartMleko_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStartMleko, Default, Default, evStartMleko())

//## event evDone()
evDone::evDone() {
    NOTIFY_EVENT_CONSTRUCTOR(evDone)
    setId(evDone_Default_id);
}

bool evDone::isTypeOf(const short id) const {
    return (evDone_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDone, Default, Default, evDone())

//## event evTestowanie()
evTestowanie::evTestowanie() {
    NOTIFY_EVENT_CONSTRUCTOR(evTestowanie)
    setId(evTestowanie_Default_id);
}

bool evTestowanie::isTypeOf(const short id) const {
    return (evTestowanie_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evTestowanie, Default, Default, evTestowanie())

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/
