/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Mlynek
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Mlynek.h
*********************************************************************/

#ifndef Mlynek_H
#define Mlynek_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Mlynek
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class Mlynek
class Mlynek : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedMlynek;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Mlynek(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Mlynek();
    
    ////    Operations    ////
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation mielenie()
    void mielenie();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string ustawienia);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    int getIlosc() const;
    
    //## auto_generated
    void setIlosc(int p_ilosc);
    
    //## auto_generated
    int getZadanaIlosc() const;
    
    //## auto_generated
    void setZadanaIlosc(int p_zadanaIlosc);
    
    ////    Attributes    ////

protected :

    int ilosc;		//## attribute ilosc
    
    int zadanaIlosc;		//## attribute zadanaIlosc
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // sendaction_2:
    //## statechart_method
    inline bool sendaction_2_IN() const;
    
    // Mielenie:
    //## statechart_method
    inline bool Mielenie_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Mlynek_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        sendaction_2 = 2,
        Mielenie = 3
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedMlynek : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Mlynek, OMAnimatedMlynek)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Mielenie_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Mlynek::rootState_IN() const {
    return true;
}

inline bool Mlynek::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Mlynek::sendaction_2_IN() const {
    return rootState_subState == sendaction_2;
}

inline bool Mlynek::Mielenie_IN() const {
    return rootState_subState == Mielenie;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Mlynek.h
*********************************************************************/
