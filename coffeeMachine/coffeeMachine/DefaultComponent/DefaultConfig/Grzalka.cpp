/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Grzalka
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Grzalka.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Grzalka.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Grzalka_Grzalka_SERIALIZE OM_NO_OP

#define Default_Grzalka_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_Grzalka_grzanie_SERIALIZE OM_NO_OP

#define Default_Grzalka_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("ustawienia", UNKNOWN2STRING(ustawienia));
//#]

//## package Default

//## class Grzalka
Grzalka::Grzalka(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Grzalka, Grzalka(), 0, Default_Grzalka_Grzalka_SERIALIZE);
    setActiveContext(this, true);
    itsSterownik = NULL;
    initStatechart();
}

Grzalka::~Grzalka() {
    NOTIFY_DESTRUCTOR(~Grzalka, false);
    cleanUpRelations();
}

std::string Grzalka::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_Grzalka_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
    return "";
    //#]
}

void Grzalka::grzanie() {
    NOTIFY_OPERATION(grzanie, grzanie(), 0, Default_Grzalka_grzanie_SERIALIZE);
    //#[ operation grzanie()
    temperatura ++;
    //#]
}

bool Grzalka::zapiszUstawienia(std::string ustawienia) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_Grzalka_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
    return true;
    //#]
}

Sterownik* Grzalka::getItsSterownik() const {
    return itsSterownik;
}

void Grzalka::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Grzalka::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Grzalka::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Grzalka::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

int Grzalka::getTemperatura() const {
    return temperatura;
}

void Grzalka::setTemperatura(int p_temperatura) {
    temperatura = p_temperatura;
}

void Grzalka::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Grzalka::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Grzalka::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void Grzalka::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczona");
        rootState_subState = Wylaczona;
        rootState_active = Wylaczona;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Grzalka::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczona
        case Wylaczona:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    OMSETPARAMS(evStart);
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczona");
                    NOTIFY_STATE_ENTERED("ROOT.Grzanie");
                    pushNullTransition();
                    rootState_subState = Grzanie;
                    rootState_active = Grzanie;
                    //#[ state Grzanie.(Entry) 
                                       grzanie();
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Grzanie
        case Grzanie:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Grzanie");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczona");
                    rootState_subState = Wylaczona;
                    rootState_active = Wylaczona;
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 2 
                    if(temperatura < 90)
                        {
                            NOTIFY_TRANSITION_STARTED("2");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Grzanie");
                            NOTIFY_STATE_ENTERED("ROOT.Grzanie");
                            pushNullTransition();
                            rootState_subState = Grzanie;
                            rootState_active = Grzanie;
                            //#[ state Grzanie.(Entry) 
                                               grzanie();
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("2");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("5");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Grzanie");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_2");
                            pushNullTransition();
                            rootState_subState = sendaction_2;
                            rootState_active = sendaction_2;
                            //#[ state sendaction_2.(Entry) 
                            itsSterownik->GEN(evDone);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("5");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_2
        case sendaction_2:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_2");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczona");
                    rootState_subState = Wylaczona;
                    rootState_active = Wylaczona;
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedGrzalka::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("temperatura", x2String(myReal->temperatura));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedGrzalka::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedGrzalka::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Grzalka::Wylaczona:
        {
            Wylaczona_serializeStates(aomsState);
        }
        break;
        case Grzalka::Grzanie:
        {
            Grzanie_serializeStates(aomsState);
        }
        break;
        case Grzalka::sendaction_2:
        {
            sendaction_2_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedGrzalka::Wylaczona_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczona");
}

void OMAnimatedGrzalka::sendaction_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_2");
}

void OMAnimatedGrzalka::Grzanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Grzanie");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Grzalka, Default, false, Modul, OMAnimatedModul, OMAnimatedGrzalka)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Grzalka.cpp
*********************************************************************/
