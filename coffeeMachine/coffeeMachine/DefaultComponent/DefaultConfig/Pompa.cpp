/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Pompa
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Pompa.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Pompa.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Pompa_Pompa_SERIALIZE OM_NO_OP

#define Default_Pompa_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_Pompa_wlewanie_SERIALIZE OM_NO_OP

#define Default_Pompa_wlewanieMleka_SERIALIZE OM_NO_OP

#define Default_Pompa_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("ustawienia", UNKNOWN2STRING(ustawienia));
//#]

//## package Default

//## class Pompa
Pompa::Pompa(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Pompa, Pompa(), 0, Default_Pompa_Pompa_SERIALIZE);
    setActiveContext(this, true);
    itsSterownik = NULL;
    initStatechart();
}

Pompa::~Pompa() {
    NOTIFY_DESTRUCTOR(~Pompa, false);
    cleanUpRelations();
}

std::string Pompa::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_Pompa_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
    return "";
    
    //#]
}

void Pompa::wlewanie() {
    NOTIFY_OPERATION(wlewanie, wlewanie(), 0, Default_Pompa_wlewanie_SERIALIZE);
    //#[ operation wlewanie()
    objetosc ++;
    //#]
}

void Pompa::wlewanieMleka() {
    NOTIFY_OPERATION(wlewanieMleka, wlewanieMleka(), 0, Default_Pompa_wlewanieMleka_SERIALIZE);
    //#[ operation wlewanieMleka()
    objetosc++;
    //#]
}

bool Pompa::zapiszUstawienia(std::string ustawienia) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_Pompa_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
    return true;
    //#]
}

Sterownik* Pompa::getItsSterownik() const {
    return itsSterownik;
}

void Pompa::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Pompa::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Pompa::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Pompa::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

int Pompa::getObjetosc() const {
    return objetosc;
}

void Pompa::setObjetosc(int p_objetosc) {
    objetosc = p_objetosc;
}

int Pompa::getZadanaObjetosc() const {
    return zadanaObjetosc;
}

void Pompa::setZadanaObjetosc(int p_zadanaObjetosc) {
    zadanaObjetosc = p_zadanaObjetosc;
}

void Pompa::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Pompa::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Pompa::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void Pompa::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczona");
        rootState_subState = Wylaczona;
        rootState_active = Wylaczona;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Pompa::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczona
        case Wylaczona:
        {
            if(IS_EVENT_TYPE_OF(evStartWoda_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczona");
                    NOTIFY_STATE_ENTERED("ROOT.Woda");
                    pushNullTransition();
                    rootState_subState = Woda;
                    rootState_active = Woda;
                    //#[ state Woda.(Entry) 
                    wlewanie();
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evStartMleko_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczona");
                    NOTIFY_STATE_ENTERED("ROOT.Mleko");
                    pushNullTransition();
                    rootState_subState = Mleko;
                    rootState_active = Mleko;
                    //#[ state Mleko.(Entry) 
                    wlewanieMleka();
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Woda
        case Woda:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("10");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Woda");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczona");
                    rootState_subState = Wylaczona;
                    rootState_active = Wylaczona;
                    NOTIFY_TRANSITION_TERMINATED("10");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 4 
                    if(objetosc < zadanaObjetosc)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            NOTIFY_TRANSITION_STARTED("4");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Woda");
                            NOTIFY_STATE_ENTERED("ROOT.Woda");
                            pushNullTransition();
                            rootState_subState = Woda;
                            rootState_active = Woda;
                            //#[ state Woda.(Entry) 
                            wlewanie();
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("4");
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            NOTIFY_TRANSITION_STARTED("5");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Woda");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_3");
                            pushNullTransition();
                            rootState_subState = sendaction_3;
                            rootState_active = sendaction_3;
                            //#[ state sendaction_3.(Entry) 
                            itsSterownik->GEN(evDone);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("5");
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Mleko
        case Mleko:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("9");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Mleko");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczona");
                    rootState_subState = Wylaczona;
                    rootState_active = Wylaczona;
                    NOTIFY_TRANSITION_TERMINATED("9");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 7 
                    if( objetosc < zadanaObjetosc)
                        {
                            NOTIFY_TRANSITION_STARTED("6");
                            NOTIFY_TRANSITION_STARTED("7");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Mleko");
                            NOTIFY_STATE_ENTERED("ROOT.Mleko");
                            pushNullTransition();
                            rootState_subState = Mleko;
                            rootState_active = Mleko;
                            //#[ state Mleko.(Entry) 
                            wlewanieMleka();
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("7");
                            NOTIFY_TRANSITION_TERMINATED("6");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("6");
                            NOTIFY_TRANSITION_STARTED("8");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Mleko");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_3");
                            pushNullTransition();
                            rootState_subState = sendaction_3;
                            rootState_active = sendaction_3;
                            //#[ state sendaction_3.(Entry) 
                            itsSterownik->GEN(evDone);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("8");
                            NOTIFY_TRANSITION_TERMINATED("6");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_3
        case sendaction_3:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("11");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_3");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczona");
                    rootState_subState = Wylaczona;
                    rootState_active = Wylaczona;
                    NOTIFY_TRANSITION_TERMINATED("11");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPompa::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("objetosc", x2String(myReal->objetosc));
    aomsAttributes->addAttribute("zadanaObjetosc", x2String(myReal->zadanaObjetosc));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedPompa::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedPompa::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Pompa::Wylaczona:
        {
            Wylaczona_serializeStates(aomsState);
        }
        break;
        case Pompa::Woda:
        {
            Woda_serializeStates(aomsState);
        }
        break;
        case Pompa::Mleko:
        {
            Mleko_serializeStates(aomsState);
        }
        break;
        case Pompa::sendaction_3:
        {
            sendaction_3_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedPompa::Wylaczona_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczona");
}

void OMAnimatedPompa::Woda_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Woda");
}

void OMAnimatedPompa::sendaction_3_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_3");
}

void OMAnimatedPompa::Mleko_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mleko");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Pompa, Default, false, Modul, OMAnimatedModul, OMAnimatedPompa)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Pompa.cpp
*********************************************************************/
