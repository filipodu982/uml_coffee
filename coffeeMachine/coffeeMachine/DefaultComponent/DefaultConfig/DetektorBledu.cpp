/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DetektorBledu
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/DetektorBledu.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "DetektorBledu.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_DetektorBledu_DetektorBledu_SERIALIZE OM_NO_OP

#define Default_DetektorBledu_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_DetektorBledu_sprawdzPoziomFusow_SERIALIZE OM_NO_OP

#define Default_DetektorBledu_sprawdzPoziomKawy_SERIALIZE OM_NO_OP

#define Default_DetektorBledu_sprawdzPoziomMleka_SERIALIZE OM_NO_OP

#define Default_DetektorBledu_sprawdzPoziomWody_SERIALIZE OM_NO_OP

#define Default_DetektorBledu_ustawStan_SERIALIZE aomsmethod->addAttribute("state", x2String((int)state));

#define Default_DetektorBledu_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("ustawienia", UNKNOWN2STRING(ustawienia));
//#]

//## package Default

//## class DetektorBledu
DetektorBledu::DetektorBledu(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(DetektorBledu, DetektorBledu(), 0, Default_DetektorBledu_DetektorBledu_SERIALIZE);
    setActiveContext(this, true);
    itsSterownik = NULL;
    initStatechart();
}

DetektorBledu::~DetektorBledu() {
    NOTIFY_DESTRUCTOR(~DetektorBledu, false);
    cleanUpRelations();
}

std::string DetektorBledu::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_DetektorBledu_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
    return "";
    //#]
}

bool DetektorBledu::sprawdzPoziomFusow() {
    NOTIFY_OPERATION(sprawdzPoziomFusow, sprawdzPoziomFusow(), 0, Default_DetektorBledu_sprawdzPoziomFusow_SERIALIZE);
    //#[ operation sprawdzPoziomFusow()
    return false;
    //#]
}

bool DetektorBledu::sprawdzPoziomKawy() {
    NOTIFY_OPERATION(sprawdzPoziomKawy, sprawdzPoziomKawy(), 0, Default_DetektorBledu_sprawdzPoziomKawy_SERIALIZE);
    //#[ operation sprawdzPoziomKawy()
    return false;
    //#]
}

bool DetektorBledu::sprawdzPoziomMleka() {
    NOTIFY_OPERATION(sprawdzPoziomMleka, sprawdzPoziomMleka(), 0, Default_DetektorBledu_sprawdzPoziomMleka_SERIALIZE);
    //#[ operation sprawdzPoziomMleka()
    return false;
    //#]
}

bool DetektorBledu::sprawdzPoziomWody() {
    NOTIFY_OPERATION(sprawdzPoziomWody, sprawdzPoziomWody(), 0, Default_DetektorBledu_sprawdzPoziomWody_SERIALIZE);
    //#[ operation sprawdzPoziomWody()
    return false;
    //#]
}

bool DetektorBledu::zapiszUstawienia(std::string ustawienia) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_DetektorBledu_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
    return true;
    //#]
}

void DetektorBledu::ustawStan(const Stany& state) {
    NOTIFY_OPERATION(ustawStan, ustawStan(const Stany&), 1, Default_DetektorBledu_ustawStan_SERIALIZE);
    //#[ operation ustawStan(Stany)
    stan = state;
    //#]
}

Sterownik* DetektorBledu::getItsSterownik() const {
    return itsSterownik;
}

void DetektorBledu::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool DetektorBledu::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void DetektorBledu::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void DetektorBledu::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

bool DetektorBledu::getBlad() const {
    return blad;
}

void DetektorBledu::setBlad(bool p_blad) {
    blad = p_blad;
}

Stany DetektorBledu::getStan() const {
    return stan;
}

void DetektorBledu::setStan(Stany p_stan) {
    stan = p_stan;
}

void DetektorBledu::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void DetektorBledu::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void DetektorBledu::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void DetektorBledu::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus DetektorBledu::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    OMSETPARAMS(evStart);
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    //#[ transition 1 
                    stan = params->state;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Detekcja");
                    pushNullTransition();
                    rootState_subState = Detekcja;
                    rootState_active = Detekcja;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Detekcja
        case Detekcja:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 3 
                    if(stan == WODA)
                        {
                            NOTIFY_TRANSITION_STARTED("2");
                            NOTIFY_TRANSITION_STARTED("3");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Detekcja");
                            NOTIFY_STATE_ENTERED("ROOT.Woda");
                            pushNullTransition();
                            rootState_subState = Woda;
                            rootState_active = Woda;
                            //#[ state Woda.(Entry) 
                            blad = sprawdzPoziomWody();
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("3");
                            NOTIFY_TRANSITION_TERMINATED("2");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 4 
                            if(stan == KAWA)
                                {
                                    NOTIFY_TRANSITION_STARTED("2");
                                    NOTIFY_TRANSITION_STARTED("4");
                                    popNullTransition();
                                    NOTIFY_STATE_EXITED("ROOT.Detekcja");
                                    NOTIFY_STATE_ENTERED("ROOT.Kawa");
                                    pushNullTransition();
                                    rootState_subState = Kawa;
                                    rootState_active = Kawa;
                                    //#[ state Kawa.(Entry) 
                                    blad = sprawdzPoziomKawy();
                                    //#]
                                    NOTIFY_TRANSITION_TERMINATED("4");
                                    NOTIFY_TRANSITION_TERMINATED("2");
                                    res = eventConsumed;
                                }
                            else
                                {
                                    //## transition 5 
                                    if(stan == MLEKO)
                                        {
                                            NOTIFY_TRANSITION_STARTED("2");
                                            NOTIFY_TRANSITION_STARTED("5");
                                            popNullTransition();
                                            NOTIFY_STATE_EXITED("ROOT.Detekcja");
                                            NOTIFY_STATE_ENTERED("ROOT.Mleko");
                                            pushNullTransition();
                                            rootState_subState = Mleko;
                                            rootState_active = Mleko;
                                            //#[ state Mleko.(Entry) 
                                            blad = sprawdzPoziomMleka();
                                            //#]
                                            NOTIFY_TRANSITION_TERMINATED("5");
                                            NOTIFY_TRANSITION_TERMINATED("2");
                                            res = eventConsumed;
                                        }
                                }
                        }
                }
            
        }
        break;
        // State Woda
        case Woda:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 6 
                    if(blad == true)
                        {
                            NOTIFY_TRANSITION_STARTED("6");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Woda");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_6");
                            pushNullTransition();
                            rootState_subState = sendaction_6;
                            rootState_active = sendaction_6;
                            //#[ state sendaction_6.(Entry) 
                            itsSterownik->GEN(evBlad);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("6");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("10");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Woda");
                            NOTIFY_STATE_ENTERED("ROOT.Fusy");
                            pushNullTransition();
                            rootState_subState = Fusy;
                            rootState_active = Fusy;
                            //#[ state Fusy.(Entry) 
                            blad = sprawdzPoziomFusow();
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("10");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Kawa
        case Kawa:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 7 
                    if(blad == true)
                        {
                            NOTIFY_TRANSITION_STARTED("7");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Kawa");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_6");
                            pushNullTransition();
                            rootState_subState = sendaction_6;
                            rootState_active = sendaction_6;
                            //#[ state sendaction_6.(Entry) 
                            itsSterownik->GEN(evBlad);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("7");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("11");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Kawa");
                            NOTIFY_STATE_ENTERED("ROOT.Fusy");
                            pushNullTransition();
                            rootState_subState = Fusy;
                            rootState_active = Fusy;
                            //#[ state Fusy.(Entry) 
                            blad = sprawdzPoziomFusow();
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("11");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Mleko
        case Mleko:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 8 
                    if(blad == true)
                        {
                            NOTIFY_TRANSITION_STARTED("8");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Mleko");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_6");
                            pushNullTransition();
                            rootState_subState = sendaction_6;
                            rootState_active = sendaction_6;
                            //#[ state sendaction_6.(Entry) 
                            itsSterownik->GEN(evBlad);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("8");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("12");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Mleko");
                            NOTIFY_STATE_ENTERED("ROOT.Fusy");
                            pushNullTransition();
                            rootState_subState = Fusy;
                            rootState_active = Fusy;
                            //#[ state Fusy.(Entry) 
                            blad = sprawdzPoziomFusow();
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("12");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Fusy
        case Fusy:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 13 
                    if(blad == true)
                        {
                            NOTIFY_TRANSITION_STARTED("13");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Fusy");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_6");
                            pushNullTransition();
                            rootState_subState = sendaction_6;
                            rootState_active = sendaction_6;
                            //#[ state sendaction_6.(Entry) 
                            itsSterownik->GEN(evBlad);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("13");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("14");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Fusy");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_7");
                            pushNullTransition();
                            rootState_subState = sendaction_7;
                            rootState_active = sendaction_7;
                            //#[ state sendaction_7.(Entry) 
                            itsSterownik->GEN(evDone);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("14");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_6
        case sendaction_6:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("9");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_6");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("9");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_7
        case sendaction_7:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("15");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_7");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("15");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDetektorBledu::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("blad", x2String(myReal->blad));
    aomsAttributes->addAttribute("stan", x2String((int)myReal->stan));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedDetektorBledu::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedDetektorBledu::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case DetektorBledu::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case DetektorBledu::Detekcja:
        {
            Detekcja_serializeStates(aomsState);
        }
        break;
        case DetektorBledu::Woda:
        {
            Woda_serializeStates(aomsState);
        }
        break;
        case DetektorBledu::Kawa:
        {
            Kawa_serializeStates(aomsState);
        }
        break;
        case DetektorBledu::Mleko:
        {
            Mleko_serializeStates(aomsState);
        }
        break;
        case DetektorBledu::Fusy:
        {
            Fusy_serializeStates(aomsState);
        }
        break;
        case DetektorBledu::sendaction_6:
        {
            sendaction_6_serializeStates(aomsState);
        }
        break;
        case DetektorBledu::sendaction_7:
        {
            sendaction_7_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedDetektorBledu::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedDetektorBledu::Woda_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Woda");
}

void OMAnimatedDetektorBledu::sendaction_7_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_7");
}

void OMAnimatedDetektorBledu::sendaction_6_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_6");
}

void OMAnimatedDetektorBledu::Mleko_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mleko");
}

void OMAnimatedDetektorBledu::Kawa_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Kawa");
}

void OMAnimatedDetektorBledu::Fusy_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Fusy");
}

void OMAnimatedDetektorBledu::Detekcja_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Detekcja");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(DetektorBledu, Default, false, Modul, OMAnimatedModul, OMAnimatedDetektorBledu)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/DetektorBledu.cpp
*********************************************************************/
