/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Modul
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Modul.h"
//#[ ignore
#define Default_Modul_Modul_SERIALIZE OM_NO_OP

#define Default_Modul_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_Modul_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("ustawienia", UNKNOWN2STRING(ustawienia));
//#]

//## package Default

//## class Modul
Modul::Modul() {
    NOTIFY_ACTIVE_NOT_REACTIVE_CONSTRUCTOR(Modul, Modul(), 0, Default_Modul_Modul_SERIALIZE);
}

Modul::~Modul() {
    NOTIFY_DESTRUCTOR(~Modul, true);
}

int Modul::getId() const {
    return id;
}

void Modul::setId(int p_id) {
    id = p_id;
}

std::string Modul::getNazwa() const {
    return nazwa;
}

void Modul::setNazwa(std::string p_nazwa) {
    nazwa = p_nazwa;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedModul::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("nazwa", UNKNOWN2STRING(myReal->nazwa));
    aomsAttributes->addAttribute("id", x2String(myReal->id));
}
//#]

IMPLEMENT_META_P(Modul, Default, Default, false, OMAnimatedModul)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/
