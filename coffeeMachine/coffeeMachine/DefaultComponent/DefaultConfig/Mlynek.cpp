/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Mlynek
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Mlynek.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Mlynek.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Mlynek_Mlynek_SERIALIZE OM_NO_OP

#define Default_Mlynek_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_Mlynek_mielenie_SERIALIZE OM_NO_OP

#define Default_Mlynek_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("ustawienia", UNKNOWN2STRING(ustawienia));
//#]

//## package Default

//## class Mlynek
Mlynek::Mlynek(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Mlynek, Mlynek(), 0, Default_Mlynek_Mlynek_SERIALIZE);
    setActiveContext(this, true);
    itsSterownik = NULL;
    initStatechart();
}

Mlynek::~Mlynek() {
    NOTIFY_DESTRUCTOR(~Mlynek, false);
    cleanUpRelations();
}

std::string Mlynek::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_Mlynek_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
    return "";
    //#]
}

void Mlynek::mielenie() {
    NOTIFY_OPERATION(mielenie, mielenie(), 0, Default_Mlynek_mielenie_SERIALIZE);
    //#[ operation mielenie()
    ilosc ++;
    //#]
}

bool Mlynek::zapiszUstawienia(std::string ustawienia) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_Mlynek_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
    return true;
    //#]
}

Sterownik* Mlynek::getItsSterownik() const {
    return itsSterownik;
}

void Mlynek::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Mlynek::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Mlynek::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Mlynek::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

int Mlynek::getIlosc() const {
    return ilosc;
}

void Mlynek::setIlosc(int p_ilosc) {
    ilosc = p_ilosc;
}

int Mlynek::getZadanaIlosc() const {
    return zadanaIlosc;
}

void Mlynek::setZadanaIlosc(int p_zadanaIlosc) {
    zadanaIlosc = p_zadanaIlosc;
}

void Mlynek::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Mlynek::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Mlynek::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void Mlynek::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Mlynek::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    OMSETPARAMS(evStart);
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Mielenie");
                    pushNullTransition();
                    rootState_subState = Mielenie;
                    rootState_active = Mielenie;
                    //#[ state Mielenie.(Entry) 
                               mielenie();
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Mielenie
        case Mielenie:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Mielenie");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 2 
                    if(   ilosc < zadanaIlosc)
                        {
                            NOTIFY_TRANSITION_STARTED("2");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Mielenie");
                            NOTIFY_STATE_ENTERED("ROOT.Mielenie");
                            pushNullTransition();
                            rootState_subState = Mielenie;
                            rootState_active = Mielenie;
                            //#[ state Mielenie.(Entry) 
                                       mielenie();
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("2");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Mielenie");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_2");
                            pushNullTransition();
                            rootState_subState = sendaction_2;
                            rootState_active = sendaction_2;
                            //#[ state sendaction_2.(Entry) 
                            itsSterownik->GEN(evDone);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_2
        case sendaction_2:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_2");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedMlynek::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("ilosc", x2String(myReal->ilosc));
    aomsAttributes->addAttribute("zadanaIlosc", x2String(myReal->zadanaIlosc));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedMlynek::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedMlynek::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Mlynek::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Mlynek::Mielenie:
        {
            Mielenie_serializeStates(aomsState);
        }
        break;
        case Mlynek::sendaction_2:
        {
            sendaction_2_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedMlynek::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedMlynek::sendaction_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_2");
}

void OMAnimatedMlynek::Mielenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mielenie");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Mlynek, Default, false, Modul, OMAnimatedModul, OMAnimatedMlynek)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Mlynek.cpp
*********************************************************************/
