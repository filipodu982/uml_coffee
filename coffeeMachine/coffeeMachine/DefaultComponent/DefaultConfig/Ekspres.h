/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ekspres
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Ekspres.h
*********************************************************************/

#ifndef Ekspres_H
#define Ekspres_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
#include <oxf/omlist.h>
//## link itsSterownik
#include "Sterownik.h"
//## link itsUstawienia
class Ustawienia;

//## package Default

//## class Ekspres
class Ekspres : public OMThread, public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedEkspres;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Ekspres(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Ekspres();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    OMIterator<Ustawienia*> getItsUstawienia() const;
    
    //## auto_generated
    Ustawienia* newItsUstawienia();
    
    //## auto_generated
    void deleteItsUstawienia(Ustawienia* p_Ustawienia);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Sterownik itsSterownik;		//## link itsSterownik
    
    OMList<Ustawienia*> itsUstawienia;		//## link itsUstawienia
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void _addItsUstawienia(Ustawienia* p_Ustawienia);
    
    //## auto_generated
    void _removeItsUstawienia(Ustawienia* p_Ustawienia);
    
    //## auto_generated
    virtual void destroy();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedEkspres : virtual public AOMInstance {
    DECLARE_META(Ekspres, OMAnimatedEkspres)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ekspres.h
*********************************************************************/
