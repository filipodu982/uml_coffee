/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Sterownik.h"
//## link itsEkspres
#include "Ekspres.h"
//#[ ignore
#define Default_Sterownik_Sterownik_SERIALIZE OM_NO_OP

#define Default_Sterownik_ustawObjetosc_SERIALIZE aomsmethod->addAttribute("volume", x2String(volume));

#define Default_Sterownik_ustawStan_SERIALIZE aomsmethod->addAttribute("state", x2String((int)state));
//#]

//## package Default

//## class Sterownik
Sterownik::Sterownik(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Sterownik, Sterownik(), 0, Default_Sterownik_Sterownik_SERIALIZE);
    setActiveContext(this, true);
    {
        {
            itsPanelSterowania.setShouldDelete(false);
        }
        {
            itsMlynek.setShouldDelete(false);
        }
        {
            itsGrzalka.setShouldDelete(false);
        }
        {
            itsPompa.setShouldDelete(false);
        }
        {
            itsSpieniacz.setShouldDelete(false);
        }
        {
            itsUbijacz.setShouldDelete(false);
        }
        {
            itsDetektorBledu.setShouldDelete(false);
        }
    }
    itsEkspres = NULL;
    initRelations();
    initStatechart();
}

Sterownik::~Sterownik() {
    NOTIFY_DESTRUCTOR(~Sterownik, true);
    cleanUpRelations();
    cancelTimeouts();
}

void Sterownik::ustawObjetosc(int volume) {
    NOTIFY_OPERATION(ustawObjetosc, ustawObjetosc(int), 1, Default_Sterownik_ustawObjetosc_SERIALIZE);
    //#[ operation ustawObjetosc(int)
    zadanaObjetosc = volume;
    //#]
}

void Sterownik::ustawStan(const Stany& state) {
    NOTIFY_OPERATION(ustawStan, ustawStan(const Stany&), 1, Default_Sterownik_ustawStan_SERIALIZE);
    //#[ operation ustawStan(Stany)
    stan = state;
    //#]
}

DetektorBledu* Sterownik::getItsDetektorBledu() const {
    return (DetektorBledu*) &itsDetektorBledu;
}

Ekspres* Sterownik::getItsEkspres() const {
    return itsEkspres;
}

void Sterownik::setItsEkspres(Ekspres* p_Ekspres) {
    _setItsEkspres(p_Ekspres);
}

Grzalka* Sterownik::getItsGrzalka() const {
    return (Grzalka*) &itsGrzalka;
}

Mlynek* Sterownik::getItsMlynek() const {
    return (Mlynek*) &itsMlynek;
}

panelSterowania* Sterownik::getItsPanelSterowania() const {
    return (panelSterowania*) &itsPanelSterowania;
}

Pompa* Sterownik::getItsPompa() const {
    return (Pompa*) &itsPompa;
}

Spieniacz* Sterownik::getItsSpieniacz() const {
    return (Spieniacz*) &itsSpieniacz;
}

Ubijacz* Sterownik::getItsUbijacz() const {
    return (Ubijacz*) &itsUbijacz;
}

bool Sterownik::startBehavior() {
    bool done = true;
    done &= itsDetektorBledu.startBehavior();
    done &= itsGrzalka.startBehavior();
    done &= itsMlynek.startBehavior();
    done &= itsPanelSterowania.startBehavior();
    done &= itsPompa.startBehavior();
    done &= itsSpieniacz.startBehavior();
    done &= itsUbijacz.startBehavior();
    done &= OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

Stany Sterownik::getStan() const {
    return stan;
}

void Sterownik::setStan(Stany p_stan) {
    stan = p_stan;
}

int Sterownik::getWybor() const {
    return wybor;
}

void Sterownik::setWybor(int p_wybor) {
    wybor = p_wybor;
}

int Sterownik::getZadanaIlosc() const {
    return zadanaIlosc;
}

void Sterownik::setZadanaIlosc(int p_zadanaIlosc) {
    zadanaIlosc = p_zadanaIlosc;
}

int Sterownik::getZadanaObjetosc() const {
    return zadanaObjetosc;
}

void Sterownik::setZadanaObjetosc(int p_zadanaObjetosc) {
    zadanaObjetosc = p_zadanaObjetosc;
}

void Sterownik::initRelations() {
    itsDetektorBledu._setItsSterownik(this);
    itsGrzalka._setItsSterownik(this);
    itsMlynek._setItsSterownik(this);
    itsPanelSterowania._setItsSterownik(this);
    itsPompa._setItsSterownik(this);
    itsSpieniacz._setItsSterownik(this);
    itsUbijacz._setItsSterownik(this);
}

void Sterownik::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    Woda_subState = OMNonState;
    Spienianie_subState = OMNonState;
    Spienianie_timeout = NULL;
    Mielenie_subState = OMNonState;
}

void Sterownik::cleanUpRelations() {
    if(itsEkspres != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsEkspres");
            itsEkspres = NULL;
        }
}

void Sterownik::cancelTimeouts() {
    cancel(Spienianie_timeout);
}

bool Sterownik::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(Spienianie_timeout == arg)
        {
            Spienianie_timeout = NULL;
            res = true;
        }
    return res;
}

void Sterownik::__setItsEkspres(Ekspres* p_Ekspres) {
    itsEkspres = p_Ekspres;
    if(p_Ekspres != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsEkspres", p_Ekspres, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsEkspres");
        }
}

void Sterownik::_setItsEkspres(Ekspres* p_Ekspres) {
    __setItsEkspres(p_Ekspres);
}

void Sterownik::_clearItsEkspres() {
    NOTIFY_RELATION_CLEARED("itsEkspres");
    itsEkspres = NULL;
}

void Sterownik::destroy() {
    itsDetektorBledu.destroy();
    itsGrzalka.destroy();
    itsMlynek.destroy();
    itsPanelSterowania.destroy();
    itsPompa.destroy();
    itsSpieniacz.destroy();
    itsUbijacz.destroy();
    OMReactive::destroy();
}

void Sterownik::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Inicjalizacja");
        rootState_subState = Inicjalizacja;
        rootState_active = Inicjalizacja;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Sterownik::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Inicjalizacja
        case Inicjalizacja:
        {
            if(IS_EVENT_TYPE_OF(evTestowanie_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Inicjalizacja");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    //#[ state Oczekiwanie.(Entry) 
                              std::cout << "Podaj wybor napoju - 0:woda, 1:espresso, 2:kawa czarna, 3:kawa biala" << std::endl;
                    std::cin >> wybor;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Oczekiwanie
        case Oczekiwanie:
        {
            res = Oczekiwanie_handleEvent();
        }
        break;
        // State sendaction_3
        case sendaction_3:
        {
            res = sendaction_3_handleEvent();
        }
        break;
        // State terminationstate_4
        case terminationstate_4:
        {
            res = terminationstate_4_handleEvent();
        }
        break;
        // State sendaction_5
        case sendaction_5:
        {
            res = sendaction_5_handleEvent();
        }
        break;
        // State Objetosc
        case Objetosc:
        {
            res = Objetosc_handleEvent();
        }
        break;
        // State sendaction_7
        case sendaction_7:
        {
            res = sendaction_7_handleEvent();
        }
        break;
        // State sendaction_9
        case sendaction_9:
        {
            res = sendaction_9_handleEvent();
        }
        break;
        // State terminationstate_10
        case terminationstate_10:
        {
            res = terminationstate_10_handleEvent();
        }
        break;
        // State sendaction_11
        case sendaction_11:
        {
            res = sendaction_11_handleEvent();
        }
        break;
        // State sendaction_12
        case sendaction_12:
        {
            res = sendaction_12_handleEvent();
        }
        break;
        // State sendaction_14
        case sendaction_14:
        {
            res = sendaction_14_handleEvent();
        }
        break;
        // State terminationstate_15
        case terminationstate_15:
        {
            res = Spienianie_handleEvent();
        }
        break;
        // State sendaction_16
        case sendaction_16:
        {
            res = sendaction_16_handleEvent();
        }
        break;
        // State sendaction_17
        case sendaction_17:
        {
            res = sendaction_17_handleEvent();
        }
        break;
        // State Objetosc
        case Spienianie_Objetosc:
        {
            res = Spienianie_Objetosc_handleEvent();
        }
        break;
        // State sendaction_19
        case sendaction_19:
        {
            res = sendaction_19_handleEvent();
        }
        break;
        
        default:
            break;
    }
    return res;
}

void Sterownik::Woda_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Woda");
    pushNullTransition();
    rootState_subState = Woda;
    //#[ state Woda.(Entry) 
    std::cout << "Wlewanie wody\n";
    //#]
    NOTIFY_TRANSITION_STARTED("2");
    //#[ transition 2 
    ustawStan(WODA);
    //#]
    NOTIFY_STATE_ENTERED("ROOT.Woda.sendaction_3");
    Woda_subState = sendaction_3;
    rootState_active = sendaction_3;
    //#[ state Woda.sendaction_3.(Entry) 
    itsDetektorBledu.GEN(evStart(stan));
    //#]
    NOTIFY_TRANSITION_TERMINATED("2");
}

void Sterownik::Woda_exit() {
    popNullTransition();
    switch (Woda_subState) {
        // State sendaction_3
        case sendaction_3:
        {
            NOTIFY_STATE_EXITED("ROOT.Woda.sendaction_3");
        }
        break;
        // State terminationstate_4
        case terminationstate_4:
        {
            NOTIFY_STATE_EXITED("ROOT.Woda.terminationstate_4");
        }
        break;
        // State sendaction_5
        case sendaction_5:
        {
            NOTIFY_STATE_EXITED("ROOT.Woda.sendaction_5");
        }
        break;
        // State Objetosc
        case Objetosc:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Woda.Objetosc");
        }
        break;
        // State sendaction_7
        case sendaction_7:
        {
            NOTIFY_STATE_EXITED("ROOT.Woda.sendaction_7");
        }
        break;
        default:
            break;
    }
    Woda_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Woda");
}

IOxfReactive::TakeEventStatus Sterownik::Woda_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            if(IS_COMPLETED(Woda) == true)
                {
                    //## transition 27 
                    if(wybor == 3)
                        {
                            NOTIFY_TRANSITION_STARTED("27");
                            Woda_exit();
                            Spienianie_entDef();
                            NOTIFY_TRANSITION_TERMINATED("27");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("29");
                            Woda_exit();
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            //#[ state Oczekiwanie.(Entry) 
                                      std::cout << "Podaj wybor napoju - 0:woda, 1:espresso, 2:kawa czarna, 3:kawa biala" << std::endl;
                            std::cin >> wybor;
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("29");
                            res = eventConsumed;
                        }
                }
        }
    else if(IS_EVENT_TYPE_OF(evBlad_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("4");
            Woda_exit();
            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
            rootState_subState = Oczekiwanie;
            rootState_active = Oczekiwanie;
            //#[ state Oczekiwanie.(Entry) 
                      std::cout << "Podaj wybor napoju - 0:woda, 1:espresso, 2:kawa czarna, 3:kawa biala" << std::endl;
            std::cin >> wybor;
            //#]
            NOTIFY_TRANSITION_TERMINATED("4");
            res = eventConsumed;
        }
    
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::terminationstate_4_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    res = Woda_handleEvent();
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_7_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evDone_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("8");
            NOTIFY_STATE_EXITED("ROOT.Woda.sendaction_7");
            NOTIFY_STATE_ENTERED("ROOT.Woda.terminationstate_4");
            Woda_subState = terminationstate_4;
            rootState_active = terminationstate_4;
            NOTIFY_TRANSITION_TERMINATED("8");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Woda_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_5_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evDone_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("6");
            NOTIFY_STATE_EXITED("ROOT.Woda.sendaction_5");
            NOTIFY_STATE_ENTERED("ROOT.Woda.Objetosc");
            pushNullTransition();
            Woda_subState = Objetosc;
            rootState_active = Objetosc;
            //#[ state Woda.Objetosc.(Entry) 
            std::cout << "Podaj objetosc napoju" << std::endl;
            int volume;
            std::cin >> volume;
            
            if(wybor == 2){
            	ustawObjetosc(volume+ 100);
            	}
            else{
            	ustawObjetosc(volume);
            	}
            //#]
            NOTIFY_TRANSITION_TERMINATED("6");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Woda_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_3_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evDone_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("5");
            NOTIFY_STATE_EXITED("ROOT.Woda.sendaction_3");
            NOTIFY_STATE_ENTERED("ROOT.Woda.sendaction_5");
            Woda_subState = sendaction_5;
            rootState_active = sendaction_5;
            //#[ state Woda.sendaction_5.(Entry) 
            itsGrzalka.GEN(evStart());
            //#]
            NOTIFY_TRANSITION_TERMINATED("5");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evBlad_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("3");
            NOTIFY_STATE_EXITED("ROOT.Woda.sendaction_3");
            NOTIFY_STATE_ENTERED("ROOT.Woda.terminationstate_4");
            Woda_subState = terminationstate_4;
            rootState_active = terminationstate_4;
            NOTIFY_TRANSITION_TERMINATED("3");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Woda_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::Objetosc_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            NOTIFY_TRANSITION_STARTED("7");
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Woda.Objetosc");
            NOTIFY_STATE_ENTERED("ROOT.Woda.sendaction_7");
            Woda_subState = sendaction_7;
            rootState_active = sendaction_7;
            //#[ state Woda.sendaction_7.(Entry) 
            itsPompa.GEN(evStartWoda);
            //#]
            NOTIFY_TRANSITION_TERMINATED("7");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Woda_handleEvent();
        }
    return res;
}

void Sterownik::Spienianie_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Spienianie");
    pushNullTransition();
    rootState_subState = Spienianie;
    //#[ state Spienianie.(Entry) 
    std::cout << "Spienianie mleka\n";
    //#]
    NOTIFY_TRANSITION_STARTED("15");
    //#[ transition 15 
    ustawStan(MLEKO);
    //#]
    NOTIFY_STATE_ENTERED("ROOT.Spienianie.sendaction_14");
    Spienianie_subState = sendaction_14;
    rootState_active = sendaction_14;
    //#[ state Spienianie.sendaction_14.(Entry) 
    itsDetektorBledu.GEN(evStart(stan));
    //#]
    NOTIFY_TRANSITION_TERMINATED("15");
}

void Sterownik::Spienianie_exit() {
    popNullTransition();
    switch (Spienianie_subState) {
        // State sendaction_14
        case sendaction_14:
        {
            NOTIFY_STATE_EXITED("ROOT.Spienianie.sendaction_14");
        }
        break;
        // State terminationstate_15
        case terminationstate_15:
        {
            NOTIFY_STATE_EXITED("ROOT.Spienianie.terminationstate_15");
        }
        break;
        // State sendaction_16
        case sendaction_16:
        {
            cancel(Spienianie_timeout);
            NOTIFY_STATE_EXITED("ROOT.Spienianie.sendaction_16");
        }
        break;
        // State sendaction_17
        case sendaction_17:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Spienianie.sendaction_17");
        }
        break;
        // State Objetosc
        case Spienianie_Objetosc:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Spienianie.Objetosc");
        }
        break;
        // State sendaction_19
        case sendaction_19:
        {
            NOTIFY_STATE_EXITED("ROOT.Spienianie.sendaction_19");
        }
        break;
        default:
            break;
    }
    Spienianie_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Spienianie");
}

IOxfReactive::TakeEventStatus Sterownik::Spienianie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 28 
            if(IS_COMPLETED(Spienianie)==true)
                {
                    NOTIFY_TRANSITION_STARTED("28");
                    Spienianie_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    //#[ state Oczekiwanie.(Entry) 
                              std::cout << "Podaj wybor napoju - 0:woda, 1:espresso, 2:kawa czarna, 3:kawa biala" << std::endl;
                    std::cin >> wybor;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("28");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(evBlad_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("17");
            Spienianie_exit();
            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
            rootState_subState = Oczekiwanie;
            rootState_active = Oczekiwanie;
            //#[ state Oczekiwanie.(Entry) 
                      std::cout << "Podaj wybor napoju - 0:woda, 1:espresso, 2:kawa czarna, 3:kawa biala" << std::endl;
            std::cin >> wybor;
            //#]
            NOTIFY_TRANSITION_TERMINATED("17");
            res = eventConsumed;
        }
    
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_19_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evDone_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("22");
            NOTIFY_STATE_EXITED("ROOT.Spienianie.sendaction_19");
            NOTIFY_STATE_ENTERED("ROOT.Spienianie.terminationstate_15");
            Spienianie_subState = terminationstate_15;
            rootState_active = terminationstate_15;
            NOTIFY_TRANSITION_TERMINATED("22");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Spienianie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_17_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            NOTIFY_TRANSITION_STARTED("20");
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Spienianie.sendaction_17");
            NOTIFY_STATE_ENTERED("ROOT.Spienianie.Objetosc");
            pushNullTransition();
            Spienianie_subState = Spienianie_Objetosc;
            rootState_active = Spienianie_Objetosc;
            //#[ state Spienianie.Objetosc.(Entry) 
            std::cout << "Podaj objetosc mleka\n";
            int volume;
            std::cin >> volume;
            ustawObjetosc(volume);
            //#]
            NOTIFY_TRANSITION_TERMINATED("20");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Spienianie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_16_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            if(getCurrentEvent() == Spienianie_timeout)
                {
                    NOTIFY_TRANSITION_STARTED("19");
                    cancel(Spienianie_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Spienianie.sendaction_16");
                    NOTIFY_STATE_ENTERED("ROOT.Spienianie.sendaction_17");
                    pushNullTransition();
                    Spienianie_subState = sendaction_17;
                    rootState_active = sendaction_17;
                    //#[ state Spienianie.sendaction_17.(Entry) 
                    itsSpieniacz.GEN(evStop);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("19");
                    res = eventConsumed;
                }
        }
    
    if(res == eventNotConsumed)
        {
            res = Spienianie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_14_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evDone_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("18");
            NOTIFY_STATE_EXITED("ROOT.Spienianie.sendaction_14");
            NOTIFY_STATE_ENTERED("ROOT.Spienianie.sendaction_16");
            Spienianie_subState = sendaction_16;
            rootState_active = sendaction_16;
            //#[ state Spienianie.sendaction_16.(Entry) 
            itsSpieniacz.GEN(evStart());
            //#]
            Spienianie_timeout = scheduleTimeout(1500, "ROOT.Spienianie.sendaction_16");
            NOTIFY_TRANSITION_TERMINATED("18");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evBlad_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("16");
            NOTIFY_STATE_EXITED("ROOT.Spienianie.sendaction_14");
            NOTIFY_STATE_ENTERED("ROOT.Spienianie.terminationstate_15");
            Spienianie_subState = terminationstate_15;
            rootState_active = terminationstate_15;
            NOTIFY_TRANSITION_TERMINATED("16");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Spienianie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::Spienianie_Objetosc_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            NOTIFY_TRANSITION_STARTED("21");
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Spienianie.Objetosc");
            NOTIFY_STATE_ENTERED("ROOT.Spienianie.sendaction_19");
            Spienianie_subState = sendaction_19;
            rootState_active = sendaction_19;
            //#[ state Spienianie.sendaction_19.(Entry) 
            itsPompa.GEN(evStartMleko);
            //#]
            NOTIFY_TRANSITION_TERMINATED("21");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Spienianie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::Oczekiwanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evStop_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("30");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.terminationstate_20");
            rootState_subState = terminationstate_20;
            rootState_active = terminationstate_20;
            NOTIFY_TRANSITION_TERMINATED("30");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evImpuls_Default_id))
        {
            //## transition 24 
            if(wybor == 0)
                {
                    NOTIFY_TRANSITION_STARTED("23");
                    NOTIFY_TRANSITION_STARTED("24");
                    NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                    Woda_entDef();
                    NOTIFY_TRANSITION_TERMINATED("24");
                    NOTIFY_TRANSITION_TERMINATED("23");
                    res = eventConsumed;
                }
            else
                {
                    //## transition 25 
                    if(wybor == 1|| wybor == 2 || wybor == 3)
                        {
                            NOTIFY_TRANSITION_STARTED("23");
                            NOTIFY_TRANSITION_STARTED("25");
                            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                            Mielenie_entDef();
                            NOTIFY_TRANSITION_TERMINATED("25");
                            NOTIFY_TRANSITION_TERMINATED("23");
                            res = eventConsumed;
                        }
                }
        }
    
    return res;
}

void Sterownik::Mielenie_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Mielenie");
    pushNullTransition();
    rootState_subState = Mielenie;
    //#[ state Mielenie.(Entry) 
    std::cout << "Mielenie kawy\n";
    //#]
    NOTIFY_TRANSITION_STARTED("9");
    //#[ transition 9 
    ustawStan(KAWA);
    //#]
    NOTIFY_STATE_ENTERED("ROOT.Mielenie.sendaction_9");
    Mielenie_subState = sendaction_9;
    rootState_active = sendaction_9;
    //#[ state Mielenie.sendaction_9.(Entry) 
    itsDetektorBledu.GEN(evStart(stan));
    //#]
    NOTIFY_TRANSITION_TERMINATED("9");
}

void Sterownik::Mielenie_exit() {
    popNullTransition();
    switch (Mielenie_subState) {
        // State sendaction_9
        case sendaction_9:
        {
            NOTIFY_STATE_EXITED("ROOT.Mielenie.sendaction_9");
        }
        break;
        // State terminationstate_10
        case terminationstate_10:
        {
            NOTIFY_STATE_EXITED("ROOT.Mielenie.terminationstate_10");
        }
        break;
        // State sendaction_11
        case sendaction_11:
        {
            NOTIFY_STATE_EXITED("ROOT.Mielenie.sendaction_11");
        }
        break;
        // State sendaction_12
        case sendaction_12:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Mielenie.sendaction_12");
        }
        break;
        default:
            break;
    }
    Mielenie_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Mielenie");
}

IOxfReactive::TakeEventStatus Sterownik::Mielenie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 26 
            if(wybor == 1 || wybor == 2 || wybor == 3 && (IS_COMPLETED(Mielenie)==true))
                {
                    NOTIFY_TRANSITION_STARTED("26");
                    Mielenie_exit();
                    Woda_entDef();
                    NOTIFY_TRANSITION_TERMINATED("26");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(evBlad_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("11");
            Mielenie_exit();
            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
            rootState_subState = Oczekiwanie;
            rootState_active = Oczekiwanie;
            //#[ state Oczekiwanie.(Entry) 
                      std::cout << "Podaj wybor napoju - 0:woda, 1:espresso, 2:kawa czarna, 3:kawa biala" << std::endl;
            std::cin >> wybor;
            //#]
            NOTIFY_TRANSITION_TERMINATED("11");
            res = eventConsumed;
        }
    
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::terminationstate_10_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    res = Mielenie_handleEvent();
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_9_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evDone_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("12");
            NOTIFY_STATE_EXITED("ROOT.Mielenie.sendaction_9");
            NOTIFY_STATE_ENTERED("ROOT.Mielenie.sendaction_11");
            Mielenie_subState = sendaction_11;
            rootState_active = sendaction_11;
            //#[ state Mielenie.sendaction_11.(Entry) 
            itsMlynek.GEN(evStart());
            //#]
            NOTIFY_TRANSITION_TERMINATED("12");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evBlad_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("10");
            NOTIFY_STATE_EXITED("ROOT.Mielenie.sendaction_9");
            NOTIFY_STATE_ENTERED("ROOT.Mielenie.terminationstate_10");
            Mielenie_subState = terminationstate_10;
            rootState_active = terminationstate_10;
            NOTIFY_TRANSITION_TERMINATED("10");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Mielenie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_12_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            NOTIFY_TRANSITION_STARTED("14");
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Mielenie.sendaction_12");
            NOTIFY_STATE_ENTERED("ROOT.Mielenie.terminationstate_10");
            Mielenie_subState = terminationstate_10;
            rootState_active = terminationstate_10;
            NOTIFY_TRANSITION_TERMINATED("14");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Mielenie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_11_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evDone_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("13");
            NOTIFY_STATE_EXITED("ROOT.Mielenie.sendaction_11");
            NOTIFY_STATE_ENTERED("ROOT.Mielenie.sendaction_12");
            pushNullTransition();
            Mielenie_subState = sendaction_12;
            rootState_active = sendaction_12;
            //#[ state Mielenie.sendaction_12.(Entry) 
            itsUbijacz.GEN(evStart());
            //#]
            NOTIFY_TRANSITION_TERMINATED("13");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Mielenie_handleEvent();
        }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSterownik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("wybor", x2String(myReal->wybor));
    aomsAttributes->addAttribute("stan", x2String((int)myReal->stan));
    aomsAttributes->addAttribute("zadanaObjetosc", x2String(myReal->zadanaObjetosc));
    aomsAttributes->addAttribute("zadanaIlosc", x2String(myReal->zadanaIlosc));
}

void OMAnimatedSterownik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsEkspres", false, true);
    if(myReal->itsEkspres)
        {
            aomsRelations->ADD_ITEM(myReal->itsEkspres);
        }
    aomsRelations->addRelation("itsPanelSterowania", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsPanelSterowania);
    aomsRelations->addRelation("itsMlynek", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsMlynek);
    aomsRelations->addRelation("itsGrzalka", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsGrzalka);
    aomsRelations->addRelation("itsPompa", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsPompa);
    aomsRelations->addRelation("itsSpieniacz", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSpieniacz);
    aomsRelations->addRelation("itsUbijacz", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsUbijacz);
    aomsRelations->addRelation("itsDetektorBledu", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDetektorBledu);
}

void OMAnimatedSterownik::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Sterownik::Inicjalizacja:
        {
            Inicjalizacja_serializeStates(aomsState);
        }
        break;
        case Sterownik::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case Sterownik::Woda:
        {
            Woda_serializeStates(aomsState);
        }
        break;
        case Sterownik::Mielenie:
        {
            Mielenie_serializeStates(aomsState);
        }
        break;
        case Sterownik::Spienianie:
        {
            Spienianie_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_20:
        {
            terminationstate_20_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::Woda_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Woda");
    switch (myReal->Woda_subState) {
        case Sterownik::sendaction_3:
        {
            sendaction_3_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_4:
        {
            terminationstate_4_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_5:
        {
            sendaction_5_serializeStates(aomsState);
        }
        break;
        case Sterownik::Objetosc:
        {
            Objetosc_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_7:
        {
            sendaction_7_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::terminationstate_4_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Woda.terminationstate_4");
}

void OMAnimatedSterownik::sendaction_7_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Woda.sendaction_7");
}

void OMAnimatedSterownik::sendaction_5_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Woda.sendaction_5");
}

void OMAnimatedSterownik::sendaction_3_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Woda.sendaction_3");
}

void OMAnimatedSterownik::Objetosc_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Woda.Objetosc");
}

void OMAnimatedSterownik::terminationstate_20_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.terminationstate_20");
}

void OMAnimatedSterownik::Spienianie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Spienianie");
    switch (myReal->Spienianie_subState) {
        case Sterownik::sendaction_14:
        {
            sendaction_14_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_15:
        {
            terminationstate_15_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_16:
        {
            sendaction_16_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_17:
        {
            sendaction_17_serializeStates(aomsState);
        }
        break;
        case Sterownik::Spienianie_Objetosc:
        {
            Spienianie_Objetosc_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_19:
        {
            sendaction_19_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::terminationstate_15_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Spienianie.terminationstate_15");
}

void OMAnimatedSterownik::sendaction_19_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Spienianie.sendaction_19");
}

void OMAnimatedSterownik::sendaction_17_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Spienianie.sendaction_17");
}

void OMAnimatedSterownik::sendaction_16_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Spienianie.sendaction_16");
}

void OMAnimatedSterownik::sendaction_14_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Spienianie.sendaction_14");
}

void OMAnimatedSterownik::Spienianie_Objetosc_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Spienianie.Objetosc");
}

void OMAnimatedSterownik::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}

void OMAnimatedSterownik::Mielenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mielenie");
    switch (myReal->Mielenie_subState) {
        case Sterownik::sendaction_9:
        {
            sendaction_9_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_10:
        {
            terminationstate_10_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_11:
        {
            sendaction_11_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_12:
        {
            sendaction_12_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::terminationstate_10_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mielenie.terminationstate_10");
}

void OMAnimatedSterownik::sendaction_9_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mielenie.sendaction_9");
}

void OMAnimatedSterownik::sendaction_12_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mielenie.sendaction_12");
}

void OMAnimatedSterownik::sendaction_11_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Mielenie.sendaction_11");
}

void OMAnimatedSterownik::Inicjalizacja_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Inicjalizacja");
}
//#]

IMPLEMENT_REACTIVE_META_P(Sterownik, Default, Default, false, OMAnimatedSterownik)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/
