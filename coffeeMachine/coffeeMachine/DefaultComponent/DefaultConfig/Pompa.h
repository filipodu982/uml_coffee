/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Pompa
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Pompa.h
*********************************************************************/

#ifndef Pompa_H
#define Pompa_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Pompa
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class Pompa
class Pompa : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPompa;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Pompa(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Pompa();
    
    ////    Operations    ////
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation wlewanie()
    void wlewanie();
    
    //## operation wlewanieMleka()
    void wlewanieMleka();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string ustawienia);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    int getObjetosc() const;
    
    //## auto_generated
    void setObjetosc(int p_objetosc);
    
    //## auto_generated
    int getZadanaObjetosc() const;
    
    //## auto_generated
    void setZadanaObjetosc(int p_zadanaObjetosc);
    
    ////    Attributes    ////

protected :

    int objetosc;		//## attribute objetosc
    
    int zadanaObjetosc;		//## attribute zadanaObjetosc
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczona:
    //## statechart_method
    inline bool Wylaczona_IN() const;
    
    // Woda:
    //## statechart_method
    inline bool Woda_IN() const;
    
    // sendaction_3:
    //## statechart_method
    inline bool sendaction_3_IN() const;
    
    // Mleko:
    //## statechart_method
    inline bool Mleko_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Pompa_Enum {
        OMNonState = 0,
        Wylaczona = 1,
        Woda = 2,
        sendaction_3 = 3,
        Mleko = 4
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPompa : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Pompa, OMAnimatedPompa)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczona_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Woda_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_3_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Mleko_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Pompa::rootState_IN() const {
    return true;
}

inline bool Pompa::Wylaczona_IN() const {
    return rootState_subState == Wylaczona;
}

inline bool Pompa::Woda_IN() const {
    return rootState_subState == Woda;
}

inline bool Pompa::sendaction_3_IN() const {
    return rootState_subState == sendaction_3;
}

inline bool Pompa::Mleko_IN() const {
    return rootState_subState == Mleko;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Pompa.h
*********************************************************************/
