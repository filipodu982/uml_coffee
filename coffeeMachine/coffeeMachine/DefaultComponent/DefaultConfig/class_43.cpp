/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_43
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/class_43.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "class_43.h"
//#[ ignore
#define Default_class_43_class_43_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class class_43
class_43::class_43() {
    NOTIFY_CONSTRUCTOR(class_43, class_43(), 0, Default_class_43_class_43_SERIALIZE);
}

class_43::~class_43() {
    NOTIFY_DESTRUCTOR(~class_43, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(class_43, Default, Default, false, OMAnimatedclass_43)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/class_43.cpp
*********************************************************************/
