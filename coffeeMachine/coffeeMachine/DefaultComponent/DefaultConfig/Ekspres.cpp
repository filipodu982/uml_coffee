/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ekspres
//!	Generated Date	: Sat, 18, Jul 2020  
	File Path	: DefaultComponent/DefaultConfig/Ekspres.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Ekspres.h"
//## link itsUstawienia
#include "Ustawienia.h"
//#[ ignore
#define Default_Ekspres_Ekspres_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Ekspres
Ekspres::Ekspres(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Ekspres, Ekspres(), 0, Default_Ekspres_Ekspres_SERIALIZE);
    setActiveContext(this, true);
    {
        {
            itsSterownik.setShouldDelete(false);
        }
    }
    initRelations();
}

Ekspres::~Ekspres() {
    NOTIFY_DESTRUCTOR(~Ekspres, true);
    cleanUpRelations();
}

Sterownik* Ekspres::getItsSterownik() const {
    return (Sterownik*) &itsSterownik;
}

OMIterator<Ustawienia*> Ekspres::getItsUstawienia() const {
    OMIterator<Ustawienia*> iter(itsUstawienia);
    return iter;
}

Ustawienia* Ekspres::newItsUstawienia() {
    Ustawienia* newUstawienia = new Ustawienia;
    newUstawienia->_setItsEkspres(this);
    itsUstawienia.add(newUstawienia);
    NOTIFY_RELATION_ITEM_ADDED("itsUstawienia", newUstawienia, true, false);
    return newUstawienia;
}

void Ekspres::deleteItsUstawienia(Ustawienia* p_Ustawienia) {
    p_Ustawienia->_setItsEkspres(NULL);
    itsUstawienia.remove(p_Ustawienia);
    NOTIFY_RELATION_ITEM_REMOVED("itsUstawienia", p_Ustawienia);
    delete p_Ustawienia;
}

bool Ekspres::startBehavior() {
    bool done = true;
    done &= itsSterownik.startBehavior();
    done &= OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Ekspres::initRelations() {
    itsSterownik._setItsEkspres(this);
}

void Ekspres::cleanUpRelations() {
    {
        OMIterator<Ustawienia*> iter(itsUstawienia);
        while (*iter){
            deleteItsUstawienia(*iter);
            iter.reset();
        }
    }
}

void Ekspres::_addItsUstawienia(Ustawienia* p_Ustawienia) {
    if(p_Ustawienia != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsUstawienia", p_Ustawienia, false, false);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsUstawienia");
        }
    itsUstawienia.add(p_Ustawienia);
}

void Ekspres::_removeItsUstawienia(Ustawienia* p_Ustawienia) {
    NOTIFY_RELATION_ITEM_REMOVED("itsUstawienia", p_Ustawienia);
    itsUstawienia.remove(p_Ustawienia);
}

void Ekspres::destroy() {
    itsSterownik.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedEkspres::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSterownik);
    aomsRelations->addRelation("itsUstawienia", true, false);
    {
        OMIterator<Ustawienia*> iter(myReal->itsUstawienia);
        while (*iter){
            aomsRelations->ADD_ITEM(*iter);
            iter++;
        }
    }
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(Ekspres, Default, Default, false, OMAnimatedEkspres)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ekspres.cpp
*********************************************************************/
